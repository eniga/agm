import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.page.html',
  styleUrls: ['./vote.page.scss'],
})
export class VotePage implements OnInit {
  today = new Date;
  banks = [
    {
      'name': 'GT Bank',
      'count': 0
    },
    {
      'name': 'Access Bank',
      'count': 0
    },
    {
      'name': 'Eco Bank',
      'count': 0
    },
    {
      'name': 'Polaris Bank',
      'count': 0
    },
    {
      'name': 'Heritage Bank',
      'count': 0
    },
    {
      'name': 'Sterling Bank',
      'count': 0
    }
  ];

  leading = '';
  constructor() { }

  ngOnInit() {
  }

  addVote(bank){
    let i = this.banks.indexOf(bank);
    let item = this.banks[i];
    item.count = item.count + 1;
    this.banks[i] = item;
    let result = Math.max.apply(Math, this.banks.map(o => { return o.count; }))
    let items = this.banks.filter(x => x.count === result);
    this.leading = items.map(x => x.name).join(', ');
  }
}
