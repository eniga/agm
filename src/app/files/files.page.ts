import { Component, OnInit } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'app-files',
  templateUrl: './files.page.html',
  styleUrls: ['./files.page.scss'],
})
export class FilesPage implements OnInit {
  today = new Date;
  files = [
    {
      'name': '2020 AGM Report'
    },
    {
      'name': '2019 AGM Report'
    },
    {
      'name': '2018 AGM Report'
    }
  ];
  
  constructor() { }

  ngOnInit() {
  }


}
