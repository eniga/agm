import { Component, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {
  @ViewChild('content', { static: true }) content: IonContent;

  message = '';
  messages: Chat[] = [];
  constructor() { }

  ngOnInit() {
  }

  ScrollToBottom(){
    this.content.scrollToBottom(1000);
  }

  SendMessage() {
    let request = new Chat;
    request.timestamp = new Date;
    request.message = this.message;
    this.messages.push(request);
    this.message = '';
    this.ScrollToBottom();
  }

}

export class Chat {
  message: string;
  timestamp: Date;
}
