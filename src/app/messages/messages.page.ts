import { Component, ViewChild, OnInit } from '@angular/core';
import { IonContent } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  @ViewChild('content', { static: true }) content: IonContent;

  message = '';
  messages: Message[] = [
    {
      title: 'Announcement',
      message: 'AGM meeting for 2020',
      timestamp: new Date(moment().format())
    },
    {
      title: 'Announcement',
      message: 'AGM meeting for 2019',
      timestamp: new Date(moment().subtract(1, 'years').calendar())
    },
    {
      title: 'Announcement',
      message: 'AGM meeting for 2018',
      timestamp: new Date(moment().subtract(2, 'years').calendar())
    }
  ];
  today = new Date;
  constructor() { }

  ngOnInit() {
  }

}

export class Message {
  title
  message: string;
  timestamp: Date;
}
